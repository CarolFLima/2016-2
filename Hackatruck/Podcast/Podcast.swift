//
//  Podcast.swift
//  PodcastMarlon
//
//  Created by Student on 2/23/17.
//  Copyright © 2017 Marlon Lima. All rights reserved.
//

import Foundation

class Podcast{

    let nome: String
    
    let nomeDaFoto: String
    
    init(nome: String, nomeDaFoto: String){
        self.nome = nome
        
        self.nomeDaFoto = nomeDaFoto
    
    }
}

class PodcastDAO{

    static func getLista() -> [Podcast]{
        
        return [
            Podcast(nome: "99Vidas", nomeDaFoto: "99vidas"),
            Podcast(nome: "canal42", nomeDaFoto: "canal42"),
            Podcast(nome: "mm", nomeDaFoto: "mm"),
            Podcast(nome: "naoouvo", nomeDaFoto: "naoouvo"),
            Podcast(nome: "nerdcast", nomeDaFoto: "nerdcast"),
            Podcast(nome: "ompdb", nomeDaFoto: "ompdb"),
            Podcast(nome: "rapaduracast", nomeDaFoto: "rapaduracast"),
            Podcast(nome: "reloading", nomeDaFoto: "reloading"),
            Podcast(nome: "scicast", nomeDaFoto: "scicast")
        ]
    }
}
