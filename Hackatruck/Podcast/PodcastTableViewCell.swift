//
//  PodcastTableViewCell.swift
//  PodcastMarlon
//
//  Created by Student on 2/23/17.
//  Copyright © 2017 Marlon Lima. All rights reserved.
//

import UIKit

class PodcastTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var episodesLabel: UILabel!
    
 
    
    @IBOutlet weak var photoImageView: UIImageView!
    
}
