//
//  ViewController.swift
//  AppMapa
//
//  Created by Student on 3/3/17.
//  Copyright © 2017 Caroline Lima. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, CLLocationManagerDelegate {

    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    
    var userLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mapView.showsUserLocation = true
        setupLocationManager()
        
        addGesture()
    }

    func setupLocationManager(){
        
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
    
    }
    
    func addGesture(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotationToMap(gestureRecognizer:)))

        longPress.minimumPressDuration = 1.0
        
        mapView.addGestureRecognizer(longPress)
        
    }
    
    func addAnnotationToMap(gestureRecognizer: UIGestureRecognizer){
        
        let touchPoint = gestureRecognizer.location(in: mapView)
        let newCoordinate: CLLocationCoordinate2D = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let newAnnotation = MKPointAnnotation()
        
        newAnnotation.coordinate = newCoordinate
        newAnnotation.title = "HackaTopster"
        newAnnotation.subtitle = String(describing: "Latitude: \(newCoordinate.latitude) Longitude: \(newCoordinate.longitude)" )
        
        mapView.addAnnotation(newAnnotation)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            
            if let location = locations.last{
                userLocation = location
                
                
                let span = MKCoordinateSpanMake(0.05, 0.05)
                
                let loc = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
                
                let region = MKCoordinateRegionMake(loc, span)
                
                mapView.setRegion(region, animated: true)
                
                
            }
            print("Localização atual do usuário: ", userLocation.coordinate)
            
            
        }
    
    }
    
    
    
    

}

