//
//  Filmes.swift
//  RevisaoTableView
//
//  Created by Student on 3/4/17.
//  Copyright © 2017 Caroline Lima. All rights reserved.
//

import Foundation

class Filme {
    
    let genero: String
    let nome: [String]
    
    init(genero: String, nome:[String]) {
        self.nome = nome
        self.genero = genero
    }
    
    
    
}

class FilmeDAO{
    static func getList() -> [Filme]{
        return [
            Filme(genero: "Drama", nome: ["Titanic", "Clube de Compras Dallas"]),
            Filme(genero: "Terror", nome:["O chamado", "O grito", "Sexta-feira 13"]),
            Filme(genero: "Fantasia", nome:["Senhor dos Anéis", "Harry Potter e o prisioneiro de Azkaban"]),
            Filme(genero: "Sci-fi", nome:["Star-trek",  "Star Wars", "Matrix"]),
            Filme(genero:"Romance", nome:["Um Amor para recordar", "A culpa é das estrelas"])
        ]
    }
}


//class FilmeDAO{
//    
//    static func getList() -> [Filme]{
//        return [
//            Filme(genero: "Drama", nome: "Titanic"),
//            Filme(genero: "Drama", nome: "Clube de Compras Dallas"),
//            Filme(genero: "Terror", nome: "O Chamado"),
//            Filme(genero: "Terror", nome: "O Grito"),
//            Filme(genero: "Terror", nome: "Sexta-feira 13")
//        ]
//    }
//    
//}
