//
//  ViewController.swift
//  RevisaoTableView
//
//  Created by Student on 3/4/17.
//  Copyright © 2017 Caroline Lima. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var filmes = FilmeDAO.getList()
    
    var filteredFilmes = [Filme]()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
    }
    
    func filterContent(for searchText: String, scope: String = "All") {
        filteredFilmes = self.filmes.filter({ filme in
            return filme.genero.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
}

extension ViewController:  UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && (searchController.searchBar.text != "") {
            return filteredFilmes.count
        }
        
        return filmes.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredFilmes[section].nome.count
        }
        
        let filmes = self.filmes[section].nome
        
        return filmes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filmeIdentifier", for: indexPath)
        
        let filme: Filme
        
        if searchController.isActive && searchController.searchBar.text != "" {
            filme = filteredFilmes[indexPath.section]
        } else {
            filme = self.filmes[indexPath.section]
        }
        
        print("\(indexPath.section)-\(indexPath.row): \(filme.nome)")
        
        cell.textLabel?.text = filme.nome[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredFilmes[section].genero
        }
        
        return filmes[section].genero
        
    }
    
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(for: searchController.searchBar.text!)
    }
}

