//
//  ViewController.swift
//  MyFirstApp
//
//  Created by Student on 2/22/17.
//  Copyright © 2017 Caroline Lima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var middleNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sayTheUserName() {
    //pegar os nomes do usuario
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text
        let lastName = lastNameTextField.text
        
        // Escrever no label
        
        fullNameLabel.text = "\(firstName)! \(middleName)! \(lastName)!"
        
        // Apagar tudo que o usuario escrever
        firstNameTextField.text = ""
        middleNameTextField.text = ""
        lastNameTextField.text = ""
    
    }

    
    
}

